<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\kalkulator;

class KalkulatorController extends Controller
{

    public function result(Request $request)
    {
        if ($request->kali) {
            $result = (int)$request->pertama * (int)$request->kedua;
        }
        if ($request->bagi) {
            if ($request->kedua == 0) {
                return response()->json("tidak bisa dilakukan");
            }
            $result = (int)$request->pertama / (int)$request->kedua;
        }
        if ($request->tambah) {
            $result = (int)$request->pertama + (int)$request->kedua;
        }
        if ($request->kurang) {
            $result = (int)$request->pertama - (int)$request->kedua;
        }
        $kal = new kalkulator($result);
        return response()->json($kal->box);
    }
}
